import Vue from 'vue';
import router from './router';
import ApolloClient from 'apollo-boost';
import VueApollo from "vue-apollo";

Vue.use(VueApollo);

const client = new ApolloClient({
  uri: 'https://rickandmortyapi.com/graphql',
});

const apolloProvider = new VueApollo({
  defaultClient: client
});

import App from './App.vue'
import vuetify from './plugins/vuetify'

new Vue({
  el: '#app',
  apolloProvider,
  router,
  vuetify,
  render: h => h(App)
})
