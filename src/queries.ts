import gql from 'graphql-tag';

export const GET_LOCATIONS = gql`
  query locations ($page: Int, $name: String) {
    locations(page: $page, filter: {name: $name}) {
      info {
        count
      }
      results {
        id,
        name,
        type,
        dimension
      }
    }
  }
`;

export const GET_CHARACTERS = gql`
  query Characters ($page: Int, $name: String, $status: String) {
    characters(page: $page, filter: {name: $name, status: $status}) {
      info {
        count
      }
      results {
        id,
        name,
        status,
        species,
        image
      }
    }
  }
`;

export const GET_CHARACTER = gql`
  query Character ($id: ID!) {
    character(id: $id) {
      id
      name
      status
      species
      image
      type
      gender
      origin {
        id
      }
      location {
        id
      }
      episode {
        id
        name
      }
      created
    }
  }
`;

export const GET_EPISODE = gql`
  query Episode ($id: ID!) {
    episode(id: $id) {
      id
      name
      air_date
      episode
      characters {
        id,
        name
      },
      created
    }
  }
`;

export const GET_EPISODES = gql`
  query Episodes ($page: Int, $name: String, $episode: String) {
    episodes(page: $page, filter: {name: $name, episode: $episode}) {
      info {
        count
      }
      results {
        id,
        name,
        episode,
        air_date
      }
    }
  }
`;
