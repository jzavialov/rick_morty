import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

export const ROUTE_HOME = 'ROUTE_HOME'
export const ROUTE_LOCATIONS = 'ROUTE_LOCATIONS'
export const ROUTE_LOCATION = 'ROUTE_LOCATION'
export const ROUTE_EPISODES = 'ROUTE_EPISODES'
export const ROUTE_EPISODE = 'ROUTE_EPISODE'
export const ROUTE_CHARACTERS = 'ROUTE_CHARACTERS'
export const ROUTE_CHARACTER = 'ROUTE_CHARACTER'

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: ROUTE_HOME,
    component: Home,
  },
  {
    path: "/locations",
    name: ROUTE_LOCATIONS,
    component: () => import("../views/Locations.vue"),
    children: [
      {
        name: ROUTE_LOCATION,
        path: '/locations/:id',
        component: () => import("../views/Locations.vue")
      }
    ]
  },
  {
    path: "/episodes",
    name: ROUTE_EPISODES,
    component: () => import("../views/Episodes.vue"),
    children: [
      {
        name: ROUTE_EPISODE,
        path: '/episodes/:id',
        component: () => import("../views/Episodes.vue")
      }
    ]
  },
  {
    path: "/characters",
    name: ROUTE_CHARACTERS,
    component: () => import("../views/Characters.vue"),
    children: [
      {
        name: ROUTE_CHARACTER,
        path: '/characters/:id',
        component: () => import("../views/Characters.vue")
      }
    ]
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
